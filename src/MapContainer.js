
import React, { Component } from 'react';
import { Map, Polygon, Marker, GoogleApiWrapper } from 'google-maps-react';
import * as geolib from 'geolib'
import TableContainer from "./Table.js"

class SimpleMap extends Component {
  state = {
    triangleCoords: [],
    arryOfTuePoints: [],
    tableshow: false,
    dataTable: null
  }
  static defaultProps = {
    center: {
      lat: 35.75,
      lng: 51.44
    },
    zoom: 13
  }
  polygonChecker = () => {

    const truePoint = this.props.APIdata.map((eachdata, index) => {
      let eachlat = eachdata.Latitude;
      let eachlng = eachdata.ongitude
      let pointInPoly = geolib.isPointInPolygon({ latitude: eachlat, longitude: eachlng }, this.state.triangleCoords)
      // console.log("pointInPoly", pointInPoly)
      if (pointInPoly == true) {
        let newAPI = [];
        this.state.dataTable.map((each) => {
          console.log("each", each);

          if (each.name != eachdata.name) {
            newAPI.push(each)
          }
        })
        this.setState((pre) => {
          return (
            {
              dataTable: newAPI,
              arryOfTuePoints: [...pre.arryOfTuePoints, eachdata],
              tableshow: true
            }
          )
        })
      }
    })
  }
  mapClicked = (mapProps, map, clickEvent) => {
    let latitude = clickEvent.latLng.lat();
    let longitude = clickEvent.latLng.lng()
    let curentposition = { lat: latitude, lng: longitude }
    this.setState((pre) => {
      return (
        {
          triangleCoords: [curentposition, ...pre.triangleCoords]
        }
      )
    })
  }
  componentDidMount() {
    console.log("didmount api state", this.props.APIdata);
    this.setState({
      dataTable: this.props.APIdata
    })
  }
  render() {
    console.log("api state", this.state.dataTable);

    const mapStyles = {
      width: '90%',
      height: "300px"
    };
    const markerPoint = this.props.APIdata.map((eachdata, index) => {
      let a = eachdata.Latitude;
      let b = eachdata.ongitude
      return <Marker key={index} position={{
        lat: a,
        lng: b
      }} />
    })
    return (
      <div className="mapAndTable">
        <div className="mapBox">
          <Map
            className=""
            google={this.props.google}
            zoom={11}
            style={mapStyles}
            initialCenter={{ lat: 35.751122, lng: 51.446666 }}
            onClick={this.mapClicked}
          >
            {markerPoint}
            {<Polygon
              paths={this.state.triangleCoords}
              strokeColor="#0000FF"
              strokeOpacity={0.8}
              strokeWeight={2}
              fillColor="#0000FF"
              fillOpacity={0.35}
              onClick={this.polygonChecker}
            />
            }
          </Map>
        </div>
        <div className="tabel">
          {this.state.tableshow && <TableContainer
            data={this.state.arryOfTuePoints}
          />}
        </div>
        <div className="tabel2">
          {this.state.dataTable && <TableContainer
            data={this.state.dataTable}
          />}
        </div>
      </div>
    );
  }
}
export default GoogleApiWrapper({
  apiKey: ('AIzaSyBE3U1YA6KF_pOL5rue0bR8HXoft0N1-5s'),
  version: 3.31
})(SimpleMap);