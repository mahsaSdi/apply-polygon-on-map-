import React from 'react';
import './App.css';
import SimpleMap from './MapContainer'

class App extends React.Component {
  state = {
    Data: null
  }
  componentDidMount() {
    fetch(`https://api.myjson.com/bins/8ycep`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({
          Data: data
        })
      })
  }
  render() {
    return (
      <div className="app">
        <header><span><img className="locationImg" src={require('./location.png')} ></img> </span><h1>apply polygon on map ...</h1></header>
        {this.state.Data &&
          <div>
            {this.state.Data && <SimpleMap APIdata={this.state.Data} />}
          </div>}
      </div>

    );
  }
}

export default App;
