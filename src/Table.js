import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";

class TableContainer extends Component {
    render() {
        let dataTabel = [];
        let newData = this.props.data.map((eachdata, i) => {
            let num = i + 1;
            num = num.toString();
            let deletIcon = <div></div>
            let eachNewData = [eachdata.address, eachdata.name, num, deletIcon]
            dataTabel.push(eachNewData)
        })
        // console.log("newdata", dataTabel);
        const columns = ["آدرس", "نام", ""];
        return (
            <div>
                <MUIDataTable
                    title={"لیست آدرس ها"}
                    data={dataTabel}
                    columns={columns}
                />
            </div>
        )
    }
}
export default TableContainer